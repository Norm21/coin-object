const coin = {
    state: 0,
    flip: function() {
        return(this.state = Math.round(Math.random()))
        // 1. One point: Randomly set your coin object's "state" property to be either 
        //    0 or 1: use "this.state" to access the "state" property on this object.
    },
    toString: function() {
        if(this.state === 1){
            return "heads";
        } else {
            return "tails";
        }
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
    },
    toHTML: function() {
        const image = document.createElement('img');
        if(this.state === 1){
            image.src = "./img/coinHead.png";
        } else{
            image.src ="./img/coinTails.png";
        }

        document.body.appendChild(image);
        return image;
    }
};
function display20Flips() {
    const results = [];
    for (let counter = 1; counter <= 20; counter++) {
        results.push(coin.toString(coin.flip()));
        
        
    }
    
    
    let box = document.createElement('div');
    box.innerHTML = results.toString();
    document.body.appendChild(box);
    console.log()
    return results
    // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
}
console.log(display20Flips())
function display20Images() {
    const results = [];
    for (let counter = 1; counter <= 20; counter++) {
        results.push(coin.toHTML(coin.flip()));
        
    }
    let box2 = document.createElement('div');
    box2.innerHTML = results;
    return results
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
}
console.log(display20Images())